package ai;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import collections.Servidor;

/**
 * 
 * Mini inteligencia artificial que tendr� que responder algunas preguntas del usuario y podr� realizar transacciones e ingresar dinero mediante este.
 *
 */
public class AgenteAI extends Servidor implements ActionAI
{
	public static final Map<String, String> P_INTER = new HashMap<>();
	{
		P_INTER.put("Hola �Como me registro?", "En el cajero deber� presionar el n�mero 1");
		P_INTER.put("�Como me registro?", "En el cajero deber� presionar el n�mero 1");
		P_INTER.put("Hola como me registro?", "En el cajero deber� presionar el n�mero 1");
		P_INTER.put("Hola �como me registro?", "En el cajero deber� presionar el n�mero 1");
		P_INTER.put("hola como me registro", "En el cajero deber� presionar el n�mero 1");
		
	}	
	
	private static final int MATCH_VALUE = 1; 
	
	public AgenteAI(int id, String nombre) 
	{
		super(id, nombre);
	}

	@Override
	public String interactuar(String recept) 
	{
		for(Entry<String, String> entry : P_INTER.entrySet())
		{
			String[] s = entry.getKey().split(" ");
			String[] r_s = recept.split(" ");
			int matcheds = 0;
			for(int i = 0; i <= MATCH_VALUE; i++)
			{
				if(i > s.length || i > r_s.length)
				{
					break;
				}
				if(s[i].equalsIgnoreCase(r_s[i]))
				{
					matcheds++;
				}
			}
			if(matcheds >= MATCH_VALUE)
			{
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	public boolean realizarTransaccion(int dniSender, int dniReceiver) 
	{
		return false;
	}

	@Override
	public boolean realizarIngresoSaldo(int dniSender, double saldo) 
	{
		return false;
	}

	
}
