package ai;

public interface ActionAI 
{
	public String interactuar(String recept);
	public boolean realizarTransaccion(int dniSender, int dniReceiver);
	public boolean realizarIngresoSaldo(int dniSender, double saldo);
}
