package collections;

public class Cliente extends Entidad
{
	private String cbu;
	private double saldo;
	
	public Cliente(int id, String nombre, String cbu, double saldo) {
		super(id, nombre,TipoEntidad.Cliente);
		this.cbu = cbu;
		this.saldo = saldo;
	}

	public String getCbu()
	{
		return cbu;
	}

	public void setCbu(String cbu) 
	{
		this.cbu = cbu;
	}

	public double getSaldo() 
	{
		return saldo;
	}

	public void setSaldo(double saldo) 
	{
		this.saldo = saldo;
	}
	
}
