package collections;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Banco
{
	private static final Map<Integer, Cliente> DATA_MAP = new ConcurrentHashMap<Integer, Cliente>();
	private static final Map<Integer, Servidor> DATA_SERVER = new  ConcurrentHashMap<Integer, Servidor>();
	private static final String patron = "%d%d%d%d-%d%d%d-%d%d%d-%d%d%d-%d%d%d%d";
	
	protected Banco()
	{
		
	}
	
	public void agregarServidor(int dni, String nombre)
	{
		DATA_SERVER.put(dni, new Servidor(dni, nombre));
	}
	
	public boolean crearUsuario(int id, String nombre, double saldo)
	{
		if(DATA_MAP.containsKey(id))
		{
			return false;
		}
		Random rnd = new Random();
		String cbu;
		String id_1 = "" + id;
		int id_n01 = Integer.parseInt("" + id_1.charAt(0));
		int id_n02 = Integer.parseInt("" + id_1.charAt(1));
		int id_n03 = Integer.parseInt("" + id_1.charAt(2));
		int id_n04 = Integer.parseInt("" + id_1.charAt(3));
		//Patron 2
		int id_n2, id_n3, id_n4;
		id_n2 = rnd.nextInt(9);
		id_n3 = rnd.nextInt(9);
		id_n4 = rnd.nextInt(9);
		//Patron 3
		String id_2 = "" + id;
		final int index_ul = id_2.length() - 1;
		final int index_p = id_2.length() - 2;
		final int index_anp = id_2.length() -3;
		int id_n5 = Integer.parseInt("" + id_2.charAt(index_ul));
		int id_n6 = Integer.parseInt("" + id_2.charAt(index_p));
		int id_n7 = Integer.parseInt("" + id_2.charAt(index_anp));
		//Patron 4
		int id_n8, id_n9, id_n10;
		id_n8 = rnd.nextInt(9);
		id_n9 = rnd.nextInt(9);
		id_n10 = rnd.nextInt(9);
		//Patron 5
		int id_n11, id_n12, id_n13, id_n14;
		id_n11 = rnd.nextInt(9);
		id_n12 = rnd.nextInt(9);
		id_n13 = rnd.nextInt(9);
		id_n14 = rnd.nextInt(9);
		//Asignacion del patron 
		cbu = String.format(patron, id_n01, id_n02, id_n03, id_n04, id_n2, id_n3, 
				id_n4, id_n5, id_n6, id_n7, id_n8,
				id_n9, id_n10, id_n11, id_n12, id_n13, id_n14);
		DATA_MAP.put(id, new Cliente(id, nombre, cbu, saldo));
		return true;
 	}
	
	public boolean crearUsuario(int id, String nombre)
	{
		return crearUsuario(id, nombre, 0.0);
	}
	
	public Cliente getCliente(int dni)
	{
		return DATA_MAP.get(dni);
	}
	
	
	public static Banco getInstance()
	{
		return SingletonHolder._instance;
	}
	
	protected static final class SingletonHolder
	{
		protected static final Banco _instance = new Banco();
	}
	
}
