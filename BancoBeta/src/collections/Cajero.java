package collections;

import java.util.Scanner;

import ai.AgenteAI;

public class Cajero 
{
	public static void main(String[] args) 
	{
		@SuppressWarnings("resource")
		final Scanner scan = new Scanner(System.in);
		AgenteAI ag = new AgenteAI(19876542, "Roberto Casta�eda");
		while(true)
		{
			System.out.println("Ingresa una de las opciones:");
			System.out.println("1 -> para crear un usuario y CBU unico.");
			System.out.println("2 -> para ingresar dinero a su cuenta.");
			System.out.println("5 -> Interactuar con un agente de respuestas automatizadas.");
			final int response = scan.nextInt();
			switch(response)
			{
			case 1:
				int dni;
				System.out.println("Digite su dni por favor.");
				dni = scan.nextInt();
				if(dni <= 0)
				{
					System.out.println("No permitimos ama�os ni lavados de dinero.");
					break;
				}
				else
				{
					System.out.println("Ahora ingrese su nombre completo para terminar la operacion");
					String nombre = scan.next();
					if(Banco.getInstance().crearUsuario(dni, nombre))
					{
						System.out.println("Pudimos crear tu usuario y CBU exitosamente, ahora podr�s hacer transacciones y recibir dinero de cualquier otro usuario: tu CBU es:");
						System.out.println(Banco.getInstance().getCliente(dni).getCbu());
						System.out.println("Si pudiste anotarlo solo da enter para saber que entendiste.");
					}
					else
					{
						System.out.println("Tu usuario ya existe no puedes tener dos cuentas en este Banco.");
						break;
					}
				}
				break;
			case 5:
				System.out.println("Hola �En que puedo ayudarte?");
				System.out.println(ag.interactuar("Como me registro"));
				break;
			}
			//Repetir ciclo infinitamente
		}
	}
}
