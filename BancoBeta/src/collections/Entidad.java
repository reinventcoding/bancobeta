package collections;

public class Entidad 
{
	private int id;
	private String nombre;
	private TipoEntidad tipo;
	
	protected Entidad(int id, String nombre, TipoEntidad tipo)
	{
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
	}

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public TipoEntidad getTipo() 
	{
		return tipo;
	}

	public void setTipo(TipoEntidad tipo) 
	{
		this.tipo = tipo;
	}
}
