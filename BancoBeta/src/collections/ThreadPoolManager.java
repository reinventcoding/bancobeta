package collections;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class ThreadPoolManager 
{
	protected  ScheduledThreadPoolExecutor scheduledThreadPool;
	
	public ThreadPoolManager() 
	{
		scheduledThreadPool = new ScheduledThreadPoolExecutor(100);
	}
	
	public ScheduledFuture<?> scheduleEffect(Runnable r, long delay)
    {
        if(scheduledThreadPool.isShutdown())
            return null;
        try
        {
            return scheduledThreadPool.scheduleAtFixedRate(r, delay, delay, TimeUnit.MILLISECONDS);
        }
        catch (RejectedExecutionException e)
        {
            return null;
        }
    }
}
